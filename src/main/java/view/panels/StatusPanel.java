package view.panels;

import global.GlobalState;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends JPanel {

    public final JLabel imageSize = new JLabel();
    public final JLabel brushSize = new JLabel();
    public final JLabel point = new JLabel();
    public final JPanel color = new JPanel();
    public final JLabel zoom = new JLabel();

    public StatusPanel() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel currentImSize = new JLabel("Size:");
        add(currentImSize);
        add(imageSize);
        JLabel currentSizeText = new JLabel("Brush size: ");
        add(currentSizeText);
        add(brushSize);

        JLabel currentZoomText = new JLabel("Zoom: ");
        add(currentZoomText);
        add(zoom);

        JLabel currentColorText = new JLabel("Color: ");
        add(currentColorText);
        add(color);

        JLabel currentPointText = new JLabel("Point: ");
        add(currentPointText);
        add(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        brushSize.setText(String.valueOf(GlobalState.getCurrentBrushSize()));
        zoom.setText((int) (GlobalState.getCurrentScale() * 100) + "%");
        color.setBackground(GlobalState.getBrushColor());
    }
}
