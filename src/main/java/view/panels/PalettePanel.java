package view.panels;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class PalettePanel extends JPanel {
    int rows = 3;
    int cols = 4;

    Color curSelectedColor = Color.BLACK;

    private CircularFifoQueue<Color> colors;

    private Map<Color, ColorPanel> colorPanels = new HashMap<>();

    Consumer<Color> onColorChanged;

    public PalettePanel(Consumer<Color> onColorChanged, CircularFifoQueue<Color> colors) {
        this.colors = colors;
        this.onColorChanged = onColorChanged;
        setPreferredSize(new Dimension(100, 75));
        var layout = new GridLayout(rows, cols, 1, 1);
        setLayout(layout);
        fillPalette();
        setVisible(true);
    }

    public void selectColor(Color c) {
        colorPanels.get(curSelectedColor).deselect();
        colorPanels.get(c).select();
        curSelectedColor = c;
        onColorChanged.accept(c);
    }

    public void notifyPaletteChanged() {
        removeAll();
        fillPalette();
        selectColor(colors.get(colors.size() - 1));
    }

    private void fillPalette() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                var color = colors.get((colors.size() - 1) - (i * cols + j));
                var colorPanel = new ColorPanel(color);
                add(colorPanel);
                colorPanels.put(color, colorPanel);
            }
        }
    }

    class ColorPanel extends JPanel {
        Color color;

        ColorPanel(Color c) {
            super();
            setVisible(true);
            color = c;

            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    selectColor(c);
                }
            });
        }

        void select() {
            setBorder(BorderFactory.createDashedBorder(null, 1, 1));
        }

        void deselect() {
            setBorder(null);
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(color);
            g2d.fillRect(1, 1, getWidth() - 2, getHeight() - 2);
        }
    }
}

