package view.panels;

import javax.swing.*;
import java.awt.*;
import java.util.function.IntConsumer;

public class SizePanel extends JPanel {

    public SizePanel(IntConsumer action, int min, int max, int initialValue) {
        super();
        JSlider slider = new JSlider(min, max, initialValue);
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        slider.setPreferredSize(new Dimension(140, 15));
        slider.setPaintTicks(true);
        slider.setMajorTickSpacing(10);
        slider.setPaintLabels(true);
        slider.addChangeListener(e -> action.accept(slider.getValue()));
        add(slider);
        setPreferredSize(new Dimension(160, 45));
        setVisible(true);
    }
}
