package view.custom_controls.mouse_adapters;

import global.GlobalState;
import view.CPPMainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

public class Brush extends BasePaintAdapter {
    protected Point prev = null;
    protected CPPMainWindow mainWindow;

    public Brush(CPPMainWindow mw) {
        this.mainWindow = mw;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        var curScale = GlobalState.getCurrentScale();
        var x = ((int) (e.getX() / curScale));
        var y = ((int) (e.getY() / curScale));
        GlobalState.updateCurrentPoint(new Point(e.getX(), e.getY()));
        if (prev == null) {
            prev = new Point(x, y);
            return;
        }
        var isEraseMod = SwingUtilities.isRightMouseButton(e);
        Point cur = new Point(x, y);
        var canvas = mainWindow.getDrawFrame().getCanvas().getImage();
        var g = (Graphics2D) canvas.getGraphics();
        if (isEraseMod)
            g.setColor(Color.WHITE);
        else
            g.setColor(GlobalState.getBrushColor());
        g.setStroke(new BasicStroke(GlobalState.getCurrentBrushSize(), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g.drawLine(cur.x, cur.y, prev.x, prev.y);
        prev = cur;
        mainWindow.changeTitle();
        mainWindow.repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        prev = null;
    }
}
