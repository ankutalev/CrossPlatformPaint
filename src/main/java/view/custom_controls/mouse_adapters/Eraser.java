package view.custom_controls.mouse_adapters;

import global.GlobalState;
import view.CPPMainWindow;

import java.awt.*;
import java.awt.event.MouseEvent;

public class Eraser extends Brush {
    public Eraser(CPPMainWindow mw) {
        super(mw);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        var curScale = GlobalState.getCurrentScale();
        var x = ((int) (e.getX() / curScale));
        var y = ((int) (e.getY() / curScale));
        if (prev == null) {
            prev = new Point(x, y);
            return;
        }
        Point cur = new Point(x, y);
        var canvas = mainWindow.getDrawFrame().getCanvas().getImage();
        var g = (Graphics2D) canvas.getGraphics();
        g.setColor(Color.WHITE);
        g.setStroke(new BasicStroke(GlobalState.getCurrentBrushSize(), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g.drawLine(cur.x, cur.y, prev.x, prev.y);
        prev = cur;
        mainWindow.repaint();
    }
}
