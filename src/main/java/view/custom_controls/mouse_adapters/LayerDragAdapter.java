package view.custom_controls.mouse_adapters;

import view.canvas.DrawArea;
import view.canvas.layers.Layer;

import java.awt.event.MouseEvent;

public class LayerDragAdapter extends BasePaintAdapter {
    Layer layer;
    DrawArea drawArea;

    public LayerDragAdapter(Layer layer, DrawArea drawArea) {
        this.drawArea = drawArea;
        this.layer = layer;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        layer.setCornerPosition(e.getX(), e.getY());
        drawArea.repaint();
    }
}
