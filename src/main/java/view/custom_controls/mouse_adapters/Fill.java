package view.custom_controls.mouse_adapters;

import global.GlobalState;
import utils.Span;

import java.awt.event.MouseEvent;
import java.util.ArrayDeque;

public class Fill extends BasePaintAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
        var c = GlobalState.getCurrentScale();
        var x = (int) (e.getX() / c);
        var y = (int) (e.getY() / c);
        var canvas = GlobalState.getDrawFrame().getCanvas().getImage();
        var spans = new ArrayDeque<Span>();
        spans.push(new Span(x,y,canvas));
        int seed = canvas.getRGB(x,y);
        int brush = GlobalState.getBrushColor().getRGB();
        if (seed==brush)
            return;
        while (!spans.isEmpty()) {
            var s = spans.pop();
            s.fillSpan(brush);
            s.addAllNeighbours(seed,spans);
        }
        GlobalState.getDrawFrame().getCanvas().repaint();
    }
    
}
