package view.custom_controls.mouse_adapters;

import global.GlobalState;

import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.awt.event.MouseEvent;

public class BasePaintAdapter extends MouseInputAdapter {
    @Override
    public void mouseMoved(MouseEvent e) {
        GlobalState.updateCurrentPoint(new Point(e.getX(), e.getY()));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        GlobalState.updateCurrentPoint(null);
    }
}
