package view.custom_controls;

import view.canvas.DrawFrame;

import javax.swing.*;
import java.awt.*;

public class JInnerDesktopPane extends JDesktopPane {
    private DrawFrame df;

    public JInnerDesktopPane(DrawFrame df) {
        this.df = df;
        add(df);
        setBackground(Color.DARK_GRAY);
    }

    @Override
    public Dimension getPreferredSize() {
        return df.getPreferredSize();
    }
}
