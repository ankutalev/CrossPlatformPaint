package view;

import cg.MainFrame;
import global.GlobalState;
import view.canvas.DrawFrame;
import view.custom_controls.JInnerDesktopPane;
import view.custom_controls.mouse_adapters.Brush;
import view.custom_controls.mouse_adapters.Eraser;
import view.custom_controls.mouse_adapters.Fill;
import view.custom_controls.mouse_adapters.Pencil;
import view.dialogs.ColorChangeDialog;
import view.dialogs.TextDialog;
import view.panels.SizePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Objects;

public class CPPMainWindow extends MainFrame {
    private static final int WIDTH = 900;
    private static final int HEIGHT = 700;
    private static final int MINIMAL_SIDE_SIZE = 450;
    private DrawFrame drawFrame;
    private static final File DEFAULT_FILE = new File("New");
    private File currentFile = DEFAULT_FILE;

    public CPPMainWindow() {
        super(WIDTH, HEIGHT, "New");
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setResizable(true);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setMinimumSize(new Dimension(MINIMAL_SIDE_SIZE, MINIMAL_SIDE_SIZE));
        try {
            setupApplicationIcon();
            fillMenu();
            fillToolbar();
            var desktopPane = setupCanvas();
            addScrollPane(desktopPane);
            pack();
            setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e,"Error during initialziation", JOptionPane.ERROR_MESSAGE);
        }

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                var dialogResult = getSaveDialogChoice();
                if (dialogResult == JOptionPane.CANCEL_OPTION)
                    return;
                if (dialogResult == JOptionPane.YES_OPTION)
                    onSave();
                dispose();
                System.exit(0);
            }
        });
    }

    private void addScrollPane(JDesktopPane jDesktopPane) {
        var sp = new JScrollPane(jDesktopPane);
        setLayout(new BorderLayout());
        add(toolBar, BorderLayout.PAGE_START);
        add(sp, BorderLayout.CENTER);
        add(GlobalState.statusPanel, BorderLayout.PAGE_END);
    }

    private JInnerDesktopPane setupCanvas() {
        drawFrame = new DrawFrame(new Brush(this));
        GlobalState.setDrawFrame(drawFrame);
        setLocationRelativeTo(null);
        return new JInnerDesktopPane(drawFrame);
    }

    private void fillToolbar() {
        addToolBarButton("File/New", "new.png");
        addToolBarButton("File/Open", "open.png");
        addToolBarButton("File/Save", "save.png");
//        addToolBarButton("File/Settings", "settings.png");
        addToolBarSeparator();
        addToolBarButton("Editor/Zoom In", "zoom_in.png");
        addToolBarButton("Editor/Zoom Out", "zoom_out.png");
        addToolBarSeparator();
        addToolBarButtonWithExpandingPanel("palette.png", GlobalState.getSmallPalettePanel());
        addToolBarButtonWithExpandingPanel("brush.png", new SizePanel(GlobalState::setBrushSize, GlobalState.MIN_BRUSH_SIZE, GlobalState.MAX_BRUSH_SIZE, GlobalState.getCurrentBrushSize()));
        addToolBarSeparator();
        addToolBarButton("Editor/Text", "text.png");
        addToolBarSeparator();
        addToolBarToggledButton("Tools/Brush", "brush_tool.png", true);
        addToolBarToggledButton("Tools/Eraser", "erase.png", false);
        addToolBarToggledButton("Tools/Pencil", "pencil_tool.png", false);
        addToolBarToggledButton("Tools/Fill", "fill.png", false);
    }

    private void setupApplicationIcon() {
        var icon = new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("dolphin.png")));
        setIconImage(icon.getImage());
    }

    private void fillMenu() throws NoSuchMethodException {
        addSubMenu("File");
        addMenuItem("File/Open", "Open file", "onFileOpen");
        addMenuItem("File/Settings", "Settings", "onSettings");
        addMenuItem("File/Save", "Save", "onSave");
        addMenuItem("File/New", "New", "onNew");
        addSubMenu("Editor");
        addMenuItem("Editor/Brush color", "Brush color", "onBrushColor");
        addMenuItem("Editor/Zoom In", "Zoom In", "onZoomIn");
        addMenuItem("Editor/Zoom Out", "Zoom Out", "onZoomOut");
        addSubMenu("Tools");
        addMenuItem("Tools/Brush", "Brush", "setBrush");
        addMenuItem("Editor/Text", "Add text", "onAddText");
        addMenuItem("Tools/Eraser", "Eraser", "setEraser");
        addMenuItem("Tools/Pencil", "Pencil", "setPencil");
        addMenuItem("Tools/Fill", "Fill", "setFill");

        addSubMenu("Help");
        addMenuItem("Help/About", "About", "onAbout");
    }

    public void setBrush() {
        drawFrame.setDrawTool(new Brush(this));
    }

    public void setEraser() {
        drawFrame.setDrawTool(new Eraser(this));
    }

    public void setPencil() {
        drawFrame.setDrawTool(new Pencil(this));
    }

    public void setFill() {
        drawFrame.setDrawTool(new Fill());
    }

    public void onFileOpen() {
        var fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            var file = fileChooser.getSelectedFile();
            currentFile = file;
            setTitle(currentFile.getName());
            drawFrame.loadImage(file);
        }
    }

    public void onSave() {
        var fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save as");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setSelectedFile(new File("Untitled"));
        int result = fileChooser.showSaveDialog(this);
        if (result != JFileChooser.APPROVE_OPTION) {
            return;
        }
        final var selectedFile = fileChooser.getSelectedFile();
        drawFrame.saveImage(selectedFile);
        setTitle(selectedFile.getName());
        currentFile = selectedFile;
    }

    public void onNew() {
        if (getSaveDialogChoice() == JOptionPane.YES_OPTION) {
            onSave();
        }
        currentFile = DEFAULT_FILE;
        setTitle(DEFAULT_FILE.getName());
        drawFrame.clear();
    }

    /**
     * @return one of three options: JOptionPane.NO_OPTION,OptionPane.YES_OPTION,OptionPane.CANCEL_OPTION
     */
    private int getSaveDialogChoice() {
        if (getTitle().equals("New") || drawFrame.compareContents(currentFile))
            return JOptionPane.NO_OPTION;
        return JOptionPane.showConfirmDialog(this, "Would you like to save current art?", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
    }

    public void onSettings() {
    }

    public void onAbout() {
    }

    public void onBrushColor() {
        new ColorChangeDialog(this);
    }

    public void onZoomIn() {
        GlobalState.changeScale(i -> i + 1);
        drawFrame.zoom();
        invalidate();
        repaint();
    }

    public void onZoomOut() {
        GlobalState.changeScale(i -> i - 1);
        drawFrame.zoom();
        invalidate();
        repaint();
    }

    public void onAddText() {
        new TextDialog(this);
    }

    public void changeTitle() {
        var curTitle = getTitle();
        if (curTitle.startsWith("*"))
            return;
        setTitle("*" + curTitle);
        invalidate();
        repaint();
    }

    public DrawFrame getDrawFrame() {
        return drawFrame;
    }

}