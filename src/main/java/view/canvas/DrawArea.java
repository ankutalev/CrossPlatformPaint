package view.canvas;

import global.GlobalState;
import view.canvas.layers.Layer;
import view.canvas.layers.TextLayer;
import view.custom_controls.mouse_adapters.BasePaintAdapter;
import view.custom_controls.mouse_adapters.LayerDragAdapter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;


public class DrawArea extends JPanel {
    BufferedImage image;
    private Color backGroundColor = Color.WHITE;
    private BasePaintAdapter currentTool;
    private Layer currentLayer;

    DrawArea(int width, int height , BasePaintAdapter tool) {
        super(true);
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, backGroundColor.getRGB());
            }
        }
        setPreferredSize(new Dimension(width, height));
        setCurrentTool(tool);
        setVisible(true);
    }

    void clear() {
        image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_ARGB);
        repaint();
    }

    void updateSize(Dimension newDim) {
        var newBuff = new BufferedImage(newDim.width, newDim.height, BufferedImage.TYPE_INT_ARGB);
        var minW = Math.min(newBuff.getWidth(), image.getWidth());
        var minH = Math.min(newBuff.getHeight(), image.getHeight());
        for (int x = 0; x < minW; x++) {
            for (int y = 0; y < minH; y++) {
                newBuff.setRGB(x, y, this.image.getRGB(x, y));
            }
        }
        image = newBuff;
        repaint();
    }

    Dimension loadImage(BufferedImage image) {
        var mergedW = Math.max(image.getWidth(), this.image.getWidth());
        var mergedH = Math.max(image.getHeight(), this.image.getHeight());
        var merged = new BufferedImage(mergedW, mergedH, BufferedImage.TYPE_INT_ARGB);
        Consumer<BufferedImage> merge = (BufferedImage im) -> {
            for (int x = 0; x < im.getWidth(); x++) {
                for (int y = 0; y < im.getHeight(); y++) {
                    merged.setRGB(x, y, im.getRGB(x, y));
                }
            }
        };
        merge.accept(this.image);
        merge.accept(image);
        this.image = merged;
        repaint();
        return new Dimension(this.image.getWidth(), this.image.getHeight());
    }

    Dimension loadImage(File imageFile) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(imageFile);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(new JFrame(), "The file is not a valid image", "Input error", JOptionPane.ERROR_MESSAGE);
        }
        if (image == null) {
            JOptionPane.showMessageDialog(new JFrame(), "The file is not a valid image", "Input error", JOptionPane.ERROR_MESSAGE);
        } else {
            return loadImage(image);
        }
        return new Dimension(this.image.getWidth(), this.image.getHeight());
    }

    public void addTextLayer(String text, Font font) {
        var layer = new TextLayer(text, font, image);
        setCurrentTool(new LayerDragAdapter(layer, this));
        currentLayer = layer;
        repaint();
    }

    private void releaseCurrentLayer() {
        currentLayer.apply();
        currentLayer = null;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        var currentScaleCoeff = GlobalState.getCurrentScale();
        final var areaWidth = image.getWidth();
        final var areaHeight = image.getHeight();
        g.drawImage(image, 0, 0, (int) (areaWidth * currentScaleCoeff),
                (int) (areaHeight * currentScaleCoeff),
                0, 0, areaWidth, areaHeight, backGroundColor, this);
        if (currentLayer != null) {
            currentLayer.apply(g);
        }
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setCurrentTool(BasePaintAdapter tool) {
        if (currentLayer != null) releaseCurrentLayer();
        removeMouseMotionListener(currentTool);
        removeMouseListener(currentTool);
        addMouseListener(tool);
        addMouseMotionListener(tool);
        currentTool = tool;
    }

    public BasePaintAdapter getCurrentPaintTool() {
        return currentTool;
    }
}
