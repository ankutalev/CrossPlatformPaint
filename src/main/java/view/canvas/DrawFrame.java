package view.canvas;

import global.GlobalState;
import utils.ImageUtils;
import view.CPPMainWindow;
import view.custom_controls.ComponentResizer;
import view.custom_controls.mouse_adapters.BasePaintAdapter;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DrawFrame extends JInternalFrame {

    private static final int DEFAULT_SIDE_SIZE = 500;
    private DrawArea canvas;

    public DrawFrame(BasePaintAdapter defaultTool) {
        super("", true, false, true, false);
        setupBorders();
        canvas = new DrawArea(DEFAULT_SIDE_SIZE, DEFAULT_SIDE_SIZE,defaultTool);
        canvas.clear();
        add(canvas);
        pack();
        initCtrlvHandler();
        setVisible(true);
        GlobalState.updateCurrentImageSize(new Dimension(DEFAULT_SIDE_SIZE, DEFAULT_SIDE_SIZE));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent e) {
                super.componentResized(e);
                var c = GlobalState.getCurrentScale();
                var newDim = new Dimension(((int) (getWidth() / c)), (int) (getHeight() / c));
                GlobalState.updateCurrentImageSize(newDim);
                canvas.updateSize(newDim);
                getParent().repaint();
            }
        });
    }

    public void setDrawTool(BasePaintAdapter tool) {
        canvas.setCurrentTool(tool);
    }

    public void clear() {
        canvas.clear();
        var size = new Dimension(DEFAULT_SIDE_SIZE, DEFAULT_SIDE_SIZE);
        setSize(size);
        setPreferredSize(size);
        repaint();
    }

    public void loadImage(File image) {
        var d = canvas.loadImage(image);
        resizeByImageDimension(d);
    }

    public void saveImage(File target) {
        try {
            ImageIO.write(canvas.image, "png", target);
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(new JFrame(), "Error during write to disk!", "Can't save", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean compareContents(File im) {
        BufferedImage fromFile;
        try {
            fromFile = ImageIO.read(im);
        } catch (IOException e) {
            return false;
        }
        if (canvas.image.getWidth() != fromFile.getWidth() || canvas.image.getHeight() != fromFile.getHeight()) {
            return false;
        }
        for (int x = 0; x < fromFile.getWidth(); x++) {
            for (int y = 0; y < fromFile.getHeight(); y++) {
                if (fromFile.getRGB(x, y) != canvas.image.getRGB(x, y))
                    return false;
            }
        }
        return true;
    }

    @Override
    public Dimension getPreferredSize() {
        var c = GlobalState.getCurrentScale();
        return new Dimension(((int) (canvas.image.getWidth() * c)), ((int) (canvas.image.getHeight() * c)));
    }

    void addAsteriskTitle() {
        JFrame f2 = (JFrame) SwingUtilities.getWindowAncestor(this);
        ((CPPMainWindow) f2).changeTitle();
    }

    private void resizeByImageDimension(Dimension d) {
        var curDim = getSize();
        var newW = Math.max(curDim.width, d.width);
        var newH = Math.max(curDim.height, d.height);
        var c = GlobalState.getCurrentScale();
        var newDim = new Dimension(((int) (newW * c)), ((int) (newH * c)));
        setSize(newDim);
        setPreferredSize(d);
    }

    private void setupBorders() {
        var bi = (BasicInternalFrameUI) getUI();
        bi.setNorthPane(null);
        var cr = new ComponentResizer();
        cr.registerComponent(this);
        setBorder(BorderFactory.createMatteBorder(0, 0, 4, 4, Color.LIGHT_GRAY));
    }

    private void initCtrlvHandler() {
        KeyStroke ctrlV = KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK, true);
        InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(ctrlV, "my_action");
        ActionMap actionMap = getActionMap();
        actionMap.put("my_action", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
                if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                    try {
                        var image = ImageUtils.toBufferedImage((Image) transferable.getTransferData(DataFlavor.imageFlavor));
                        resizeByImageDimension(canvas.loadImage(image).getSize());
                        addAsteriskTitle();
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                }
            }
        });
    }

    public void zoom() {
        setSize(getPreferredSize());
    }

    public DrawArea getCanvas() {
        return canvas;
    }
}

