package view.canvas.layers;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TextLayer implements Layer {
    private BufferedImage textImage;
    private BufferedImage container;
    private int curX;
    private int curY;

    public TextLayer(String text, Font font, BufferedImage container) {
        this.container = container;

        // Measure font metrics for particular string
        Graphics2D g = (Graphics2D) container.getGraphics();
        g.setFont(font);
        int width = g.getFontMetrics().charsWidth(text.toCharArray(), 0, text.length()) + 2;
        int height = g.getFontMetrics().getHeight() * 2;
        g.dispose();

        // Create layer image with this size
        textImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = (Graphics2D) textImage.getGraphics();
        g2d.setColor(Color.BLACK);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
                RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        g2d.setFont(font);
        g2d.drawString(text, 0, height / 2);
        g2d.dispose();
    }

    @Override
    public void apply() {
        Graphics2D g2d = (Graphics2D) container.getGraphics();
        g2d.drawImage(textImage, curX, curY, null);
        g2d.dispose();
    }

    @Override
    public void apply(Graphics g) {
        g.drawImage(textImage, curX, curY, null);
    }


    @Override
    public void setCornerPosition(int x, int y) {
        curX = x;
        curY = y;
    }

}
