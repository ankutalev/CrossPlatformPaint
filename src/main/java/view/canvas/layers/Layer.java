package view.canvas.layers;

import java.awt.*;

public interface Layer {
    void apply();

    void apply(Graphics g);

    void setCornerPosition(int x, int y);
}
