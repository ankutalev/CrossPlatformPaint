package view.dialogs;

import global.GlobalState;

import javax.swing.*;
import javax.swing.colorchooser.AbstractColorChooserPanel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ColorChangeDialog extends JDialog {
    public ColorChangeDialog(Component parent) {
        setTitle("Brush color");
        setPreferredSize(new Dimension(300, 230));

        JColorChooser colorChooser = new JColorChooser();
        var hsvChooserPanel = colorChooser.getChooserPanels()[1];
        hsvChooserPanel.setColorTransparencySelectionEnabled(false);
        hsvChooserPanel.setPreferredSize(new Dimension(280, 150));
        colorChooser.setChooserPanels(new AbstractColorChooserPanel[]{hsvChooserPanel});
        colorChooser.setPreviewPanel(new JPanel());

        setLayout(new FlowLayout());
        add(colorChooser);

        var buttonPanel = new JPanel();
        var addToPaletteButton = new JButton();
        addToPaletteButton.setText("Add to palette");
        addToPaletteButton.setPreferredSize(new Dimension(150, 30));
        buttonPanel.setPreferredSize(new Dimension(280, 30));
        buttonPanel.add(addToPaletteButton);
        var cancelButton = new JButton("Cancel");
        buttonPanel.add(cancelButton);

        add(buttonPanel);

        addToPaletteButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                GlobalState.addColorToHistory(colorChooser.getColor());
                GlobalState.setBrushColor(colorChooser.getColor());
            }
        });

        cancelButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
            }
        });

        pack();
        setLocationRelativeTo(parent);
        setModal(true);
        setVisible(true);
    }
}
