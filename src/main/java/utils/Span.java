package utils;

import java.awt.image.BufferedImage;
import java.util.Deque;

public class Span {
    private final int yCoord;
    private final BufferedImage drawImage;
    private int leftBorder;
    private int rightBorder;

    public Span(int seedX, int seedY, BufferedImage drawImage) {
        this.drawImage = drawImage;
        this.yCoord = seedY;
        this.leftBorder = seedX;
        this.rightBorder = seedX;
        var seedColor = drawImage.getRGB(seedX, seedY);
        while (leftBorder >= 0 && drawImage.getRGB(leftBorder, seedY) == seedColor) {
            leftBorder--;
        }
        leftBorder++;
        while (rightBorder < drawImage.getWidth() && drawImage.getRGB(rightBorder, seedY) == seedColor) {
            rightBorder++;
        }
        if (rightBorder != seedX)
            rightBorder--;
    }

    int getRightBorder() {
        return rightBorder;
    }

    public void fillSpan(int fillColor) {
        for (int i = leftBorder; i <= rightBorder; i++) {
            drawImage.setRGB(i, yCoord, fillColor);
        }
    }

    public void addAllNeighbours(int seedColor, Deque<Span> neighbours) {
        if (yCoord != drawImage.getHeight() - 1)
            getOneSideNeighbours(yCoord + 1, neighbours, seedColor);
        if (yCoord != 0)
            getOneSideNeighbours(yCoord - 1, neighbours, seedColor);
    }

    private void getOneSideNeighbours(int yCoord, Deque<Span> neighbours, int seedColor) {
        for (int i = leftBorder; i <= rightBorder; i++) {
            if (drawImage.getRGB(i, yCoord) == seedColor) {
                var neighbour = new Span(i, yCoord, drawImage);
                i = neighbour.getRightBorder();
                neighbours.add(neighbour);
            }
        }
    }
}