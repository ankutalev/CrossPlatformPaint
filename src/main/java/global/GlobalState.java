package global;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import view.canvas.DrawFrame;
import view.panels.PalettePanel;
import view.panels.StatusPanel;

import java.awt.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.IntUnaryOperator;

public class GlobalState {
    public static final StatusPanel statusPanel = new StatusPanel();
    public static final Path dataDir = Paths.get(System.getProperty("user.dir"));
    private static final List<Double> scales = List.of(0.125d, 0.25d, 0.5d, 1d, 2d, 3d, 4d);
    private static int currentScaleIndex = 3;
    private static Color brushColor = Color.cyan;

    private GlobalState() {
    }

    private static int currentBrushSize = 2;
    public static final int MIN_BRUSH_SIZE = 0;
    public static final int MAX_BRUSH_SIZE = 50;
    private static final CircularFifoQueue<Color> recentColors = new CircularFifoQueue<>(12);

    private static final List<Color> initialColors = List.of(Color.BLACK, Color.WHITE, Color.YELLOW, Color.RED,
            Color.BLUE, Color.CYAN, Color.MAGENTA, Color.PINK,
            Color.GREEN, Color.GRAY, Color.DARK_GRAY, Color.ORANGE);

    private static String[] supportedFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

    private static Dimension currentCanvasSize;

    static {
        recentColors.addAll(initialColors);
    }

    public static void changeScale(IntUnaryOperator f) {
        currentScaleIndex = f.applyAsInt(currentScaleIndex);
        if (currentScaleIndex < 0)
            currentScaleIndex = 0;
        if (currentScaleIndex == scales.size())
            currentScaleIndex--;
    }

    public static double getCurrentScale() {
        return scales.get(currentScaleIndex);
    }

    private static PalettePanel smallPalettePanel = new PalettePanel(color -> {
        brushColor = color;
        statusPanel.color.setBackground(brushColor);
    }, recentColors);

    private static DrawFrame drawFrame;

    public static void setDrawFrame(DrawFrame frame) {
        drawFrame = frame;
    }

    public static DrawFrame getDrawFrame() {
        return drawFrame;
    }

    public static PalettePanel getSmallPalettePanel() {
        return smallPalettePanel;
    }

    public static void addColorToHistory(Color c) {
        recentColors.add(c);
        smallPalettePanel.notifyPaletteChanged();
    }

    public static Color getBrushColor() {
        return brushColor;
    }

    public static void setBrushColor(Color c) {
        statusPanel.color.setBackground(c);
        brushColor = c;
    }

    public static String[] getSupportedFonts() {
        return supportedFonts;
    }

    public static void addTextElementToCanvas(String text, Font font) {
        drawFrame.getCanvas().addTextLayer(text, font);
    }

    public static void setBrushSize(Integer val) {
        statusPanel.brushSize.setText(String.valueOf(val));
        currentBrushSize = val;
    }

    public static void updateCurrentImageSize(Dimension dim) {
        currentCanvasSize = dim;
        statusPanel.imageSize.setText(dim.width + ":" + dim.height);
    }

    public static void updateCurrentPoint(Point p) {
        if (p == null) {
            statusPanel.point.setText("");
            return;
        }
        var x = (int) (p.x / getCurrentScale());
        var y = (int) (p.y / getCurrentScale());
        if (x > currentCanvasSize.width || x < 0 || y < 0 || y > currentCanvasSize.height)
            statusPanel.point.setText("");
        else
            statusPanel.point.setText(x + ":" + y);
    }

    public static int getCurrentBrushSize() {
        return currentBrushSize;
    }
}

