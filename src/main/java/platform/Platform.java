package platform;


import javax.swing.*;

public class Platform {

    private enum PlatformType {
        WINDOWS, LINUX, MAC, UNSUPPORTED
    }

    public static final PlatformType osType = getOS();

    private static PlatformType getOS() {
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            return PlatformType.WINDOWS;
        } else if (operSys.contains("nix") || operSys.contains("nux")
                || operSys.contains("aix")) {
            return PlatformType.LINUX;
        } else if (operSys.contains("mac")) {
            return PlatformType.MAC;
        }
        return PlatformType.UNSUPPORTED;
    }

    public static void setLookAndFeel() throws Exception {
        switch (osType) {
            case MAC:
                System.setProperty("apple.laf.useScreenMenuBar", "true");
                System.setProperty("com.apple.mrj.application.apple.menu.about.name", "WikiTeX");
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                break;
            case WINDOWS:
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                break;
            default:
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
    }
}
