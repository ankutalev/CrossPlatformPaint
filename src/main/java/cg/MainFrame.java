package cg;

import platform.Platform;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.Dimension;
import java.io.File;
import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.*;

/**
 * MainFrame - main application frame
 * Application should subclass it to create its own implementation
 *
 * @author Tagir F. Valeev
 * @co-author Alena A. Bastrykina
 */
public class MainFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final String MENU_PATH_NOT_FOUND = "Menu path not found: ";
    private JMenuBar mainMenuBar;
    private List<JToggleButton> toolsButtons = new ArrayList<>();
    private JToggleButton lastToggled;
    protected JToolBar toolBar;

    /**
     * Default constructor which sets up L&F and creates tool-bar and menu-bar
     */
    public MainFrame() {
        try {
            Platform.setLookAndFeel();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Cant set OS Look And Feel", "OS error", JOptionPane.ERROR_MESSAGE);
        }
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        mainMenuBar = new JMenuBar();
        setJMenuBar(mainMenuBar);
        toolBar = new JToolBar();
        toolBar.setRollover(true);
        add(toolBar, BorderLayout.PAGE_START);
    }

    /**
     * Constructor where you can specify window size and title
     *
     * @param x     - horizontal size of newly created window
     * @param y     - vertical size of newly created window
     * @param title - window title
     */
    public MainFrame(int x, int y, String title) {
        this();
        setSize(x, y);
        setLocationByPlatform(true);
        setTitle(title);
    }

    /**
     * Shortcut method to create menu item
     * Note that you have to insert it into proper place by yourself
     *
     * @param title        - menu item title
     * @param tooltip      - floating tooltip describing menu item
     * @param mnemonic     - mnemonic key to activate item via keyboard
     * @param icon         - file name containing icon (must be located in 'resources' subpackage relative to your implementation of MainFrame), can be null
     * @param actionMethod - String containing method name which will be called when menu item is activated (method should not take any parameters)
     * @return created menu item
     * @throws NoSuchMethodException - when actionMethod method not found
     * @throws SecurityException     - when actionMethod method is inaccessible
     */
    public JMenuItem createMenuItem(String title, String tooltip, int mnemonic, String icon, String actionMethod) throws NoSuchMethodException {
        JMenuItem item = new JMenuItem(title);
        item.setMnemonic(mnemonic);
        item.setToolTipText(tooltip);
        if (icon != null)
            item.setIcon(new ImageIcon(getClass().getResource("resources/" + icon), title));
        final Method method = getClass().getMethod(actionMethod);
        item.addActionListener(evt -> {
            try {
                method.invoke(MainFrame.this);
            } catch (Exception e) {
                throw new SecurityException(e);
            }
        });
        return item;
    }

    /**
     * Shortcut method to create menu item (without icon)
     * Note that you have to insert it into proper place by yourself
     *
     * @param title        - menu item title
     * @param tooltip      - floating tooltip describing menu item
     * @param mnemonic     - mnemonic key to activate item via keyboard
     * @param actionMethod - String containing method name which will be called when menu item is activated (method should not take any parameters)
     * @return created menu item
     * @throws NoSuchMethodException - when actionMethod method not found
     * @throws SecurityException     - when actionMethod method is inaccessible
     */
    public JMenuItem createMenuItem(String title, String tooltip, int mnemonic, String actionMethod) throws NoSuchMethodException {
        return createMenuItem(title, tooltip, mnemonic, null, actionMethod);
    }

    /**
     * Creates submenu and returns it
     *
     * @param title    - submenu title
     * @param mnemonic - mnemonic key to activate submenu via keyboard
     * @return created submenu
     */
    public JMenu createSubMenu(String title, int mnemonic) {
        JMenu menu = new JMenu(title);
        menu.setMnemonic(mnemonic);
        return menu;
    }

    /**
     * Creates submenu and inserts it to the specified location
     *
     * @param title    - submenu title with full path (just submenu title for top-level submenus)
     *                 example: "File/New" - will create submenu "New" under menu "File" (provided that menu "File" was previously created)
     * @param mnemonic - mnemonic key to activate submenu via keyboard
     */
    public void addSubMenu(String title, int mnemonic) {
        MenuElement element = getParentMenuElement(title);
        if (element == null)
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
        JMenu subMenu = createSubMenu(getMenuPathName(title), mnemonic);
        if (element instanceof JMenuBar)
            ((JMenuBar) element).add(subMenu);
        else if (element instanceof JMenu)
            ((JMenu) element).add(subMenu);
        else if (element instanceof JPopupMenu)
            ((JPopupMenu) element).add(subMenu);
        else
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
    }

    /**
     * Creates menu item and adds it to the specified menu location
     *
     * @param title        - menu item title with full path
     * @param tooltip      - floating tooltip describing menu item
     * @param mnemonic     - mnemonic key to activate item via keyboard
     * @param icon         - file name containing icon (must be located in 'resources' subpackage relative to your implementation of MainFrame), can be null
     * @param actionMethod - String containing method name which will be called when menu item is activated (method should not take any parameters)
     * @throws NoSuchMethodException     - when actionMethod method not found
     * @throws SecurityException         - when actionMethod method is inaccessible
     * @throws InvalidParameterException - when specified menu location not found
     */
    public void addMenuItem(String title, String tooltip, int mnemonic, String icon, String actionMethod) throws NoSuchMethodException{
        MenuElement element = getParentMenuElement(title);
        if (element == null)
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
        JMenuItem item = createMenuItem(getMenuPathName(title), tooltip, mnemonic, icon, actionMethod);
        if (element instanceof JMenu)
            ((JMenu) element).add(item);
        else if (element instanceof JPopupMenu)
            ((JPopupMenu) element).add(item);
        else
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
    }

    /**
     * Creates menu item (without icon) and adds it to the specified menu location
     *
     * @param title        - menu item title with full path
     * @param tooltip      - floating tooltip describing menu item
     * @param mnemonic     - mnemonic key to activate item via keyboard
     * @param actionMethod - String containing method name which will be called when menu item is activated (method should not take any parameters)
     * @throws NoSuchMethodException     - when actionMethod method not found
     * @throws SecurityException         - when actionMethod method is inaccessible
     * @throws InvalidParameterException - when specified menu location not found
     */
    public void addMenuItem(String title, String tooltip, int mnemonic, String actionMethod) throws NoSuchMethodException {
        addMenuItem(title, tooltip, mnemonic, null, actionMethod);
    }

    /**
     * Adds menu separator in specified menu location
     *
     * @param title - menu location
     * @throws InvalidParameterException - when specified menu location not found
     */
    public void addMenuSeparator(String title) {
        MenuElement element = getMenuElement(title);
        if (element == null)
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
        if (element instanceof JMenu)
            ((JMenu) element).addSeparator();
        else if (element instanceof JPopupMenu)
            ((JPopupMenu) element).addSeparator();
        else
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + title);
    }

    private String getMenuPathName(String menuPath) {
        int pos = menuPath.lastIndexOf('/');
        if (pos > 0)
            return menuPath.substring(pos + 1);
        else
            return menuPath;
    }

    /**
     * Looks for menu element by menu path ignoring last path component
     *
     * @param menuPath - '/'-separated path to menu item (example: "Help/About...")
     * @return found menu item or null if no such item found
     */
    public MenuElement getParentMenuElement(String menuPath) {
        int pos = menuPath.lastIndexOf('/');
        if (pos > 0)
            return getMenuElement(menuPath.substring(0, pos));
        else
            return mainMenuBar;
    }

    /**
     * Looks for menu element by menu path
     *
     * @param menuPath - '/'-separated path to menu item (example: "Help/About...")
     * @return found menu item or null if no such item found
     */
    public MenuElement getMenuElement(String menuPath) {
        MenuElement element = mainMenuBar;
        for (String pathElement : menuPath.split("/")) {
            MenuElement newElement = null;
            for (MenuElement subElement : element.getSubElements()) {
                if ((subElement instanceof JMenu && ((JMenu) subElement).getText().equals(pathElement))
                        || (subElement instanceof JMenuItem && ((JMenuItem) subElement).getText().equals(pathElement))) {
                    newElement = subElement.getSubElements().length == 1 && subElement.getSubElements()[0] instanceof JPopupMenu ? subElement.getSubElements()[0] : subElement;
                    break;
                }
            }
            if (newElement == null) return null;
            element = newElement;
        }
        return element;
    }

    /**
     * Creates toolbar button which will behave exactly like specified menuitem
     *
     * @param item - menuitem to create toolbar button from
     * @return created toolbar button
     */
    public JButton createToolBarButton(JMenuItem item) {
        JButton button = new JButton(item.getIcon());
        for (ActionListener listener : item.getActionListeners())
            button.addActionListener(listener);
        button.setToolTipText(item.getToolTipText());
        return button;
    }

    /**
     * Creates toolbar button which will behave exactly like specified menuitem
     *
     * @param menuPath - path to menu item to create toolbar button from
     * @return created toolbar button
     */
    public JButton createToolBarButton(String menuPath) {
        JMenuItem item = (JMenuItem) getMenuElement(menuPath);
        if (item == null)
            throw new InvalidParameterException(MENU_PATH_NOT_FOUND + menuPath);
        return createToolBarButton(item);
    }

    public void addMenuItem(String path, String tooltip, String actionMethod) throws NoSuchMethodException {
        addMenuItem(path, tooltip, 0, null, actionMethod);
    }

    public void addToolBarToggledButton(String menuPath, String icon, boolean toggled) {
        JMenuItem item = (JMenuItem) getMenuElement(menuPath);
        JToggleButton b = new JToggleButton(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(icon))), toggled);
        b.setFocusPainted(false);
        if (toggled)
            lastToggled = b;
        for (ActionListener listener : item.getActionListeners())
            b.addActionListener(listener);
        b.addActionListener(e -> { if (lastToggled.equals(b)) { b.setSelected(true); } });
        b.addActionListener(e -> lastToggled = b);
        b.addActionListener(e -> toolsButtons.forEach(x -> { if (!x.getToolTipText().equals(b.getToolTipText())) x.setSelected(false); }));
        b.setToolTipText(item.getToolTipText());
        toolsButtons.add(b);
        toolBar.add(b);
    }


    public void addToolBarButtonWithText(String path, String text) {
        JButton b = createToolBarButton(path);
        b.setPreferredSize(new Dimension(24, 24));

        b.setFont(new Font("Times New Roman", Font.PLAIN & Font.CENTER_BASELINE, 22));
        b.setText(text);
        toolBar.add(b);
    }

    public void addToolBarButtonWithExpandingPanel(String icon, JPanel panel) {
        JToggleButton b = new JToggleButton();
        b.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(icon))));
        toolBar.add(b);

        var buttonLocation = b.getLocation();
        final int menuX = (int) buttonLocation.getX();
        final int menuY = (int) buttonLocation.getY() + (int) b.getPreferredSize().getHeight();

        final JPopupMenu popup = new JPopupMenu();
        popup.add(panel);

        b.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                popup.show(e.getComponent(), menuX, menuY);
            }
        });
    }

    public void addSubMenu(String title) {
        addSubMenu(title, 0);
    }

    public void addToolBarButton(String path, String icon) {
        JButton b = createToolBarButton(path);
        b.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource(icon))));
        b.setBorderPainted(false);
        toolBar.add(b);
    }

    /**
     * Creates toolbar button which will behave exactly like specified menuitem and adds it to the toolbar
     *
     * @param menuPath - path to menu item to create toolbar button from
     */
    public void addToolBarButton(String menuPath) {
        toolBar.add(createToolBarButton(menuPath));
    }

    /**
     * Adds separator to the toolbar
     */
    public void addToolBarSeparator() {
        toolBar.addSeparator();
    }

    /**
     * Prompts user for file name to save and returns it
     *
     * @param extension   - preferred file extension (example: "txt")
     * @param description - description of specified file type (example: "Text files")
     * @return File specified by user or null if user canceled operation
     */
    public File getSaveFileName(String extension, String description) {
        return FileUtils.getSaveFileName(this, extension, description);
    }

    /**
     * Prompts user for file name to open and returns it
     *
     * @param extension   - preferred file extension (example: "txt")
     * @param description - description of specified file type (example: "Text files")
     * @return File specified by user or null if user canceled operation
     */
    public File getOpenFileName(String extension, String description) {
        return FileUtils.getOpenFileName(this, extension, description);
    }
}
